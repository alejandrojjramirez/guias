<head>
    <meta charset="UTF-8">
    <title>Informacion Personal </title>
</head>
[10:45, 12/2/2020] Alejandro:<?php
$fecha_de_nacimiento = "1999-05-27";
$fecha_actual = date ("Y-m-d");
//$fecha_actual = date ("2020-02-12"); //para pruebas
echo "<br>Hoy es $fecha_actual";
echo "<br>Naciste el $fecha_de_nacimiento";
// separamos en partes las fechas
$array_nacimiento = explode ( "-", $fecha_de_nacimiento );
$array_actual = explode ( "-", $fecha_actual );
$anos =  $array_actual[0] - $array_nacimiento[0]; // calculamos años
$meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses
$dias =  $array_actual[2] - $array_nacimiento[2]; // calculamos días
//ajuste de posible negativo en $días
if ($dias < 0)
{
    --$meses;
    //ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual
    switch ($array_actual[1]) {
        case 1:     $dias_mes_anterior=31; break;
        case 2:     $dias_mes_anterior=31; break;
        case 3: 
             if (bisiesto($array_actual[0]))
             {
                 $dias_mes_anterior=29; break;
             } else {
                 $dias_mes_anterior=28; break;
             }
        case 4:     $dias_mes_anterior=31; break;
        case 5:     $dias_mes_anterior=30; break;
        case 6:     $dias_mes_anterior=31; break;
        case 7:     $dias_mes_anterior=30; break;
        case 8:     $dias_mes_anterior=31; break;
        case 9:     $dias_mes_anterior=31; break;
        case 10:     $dias_mes_anterior=30; break;
        case 11:     $dias_mes_anterior=31; break;
        case 12:     $dias_mes_anterior=30; break;
 }
 $dias=$dias + $dias_mes_anterior;
}
//ajuste de posible negativo en $meses
if ($meses < 0)
{
    --$anos;
    $meses=$meses + 12;
}
echo "<br>Tu edad es: $anos años con $meses meses y $dias días";
function bisiesto($anio_actual){
    $bisiesto=false;
    //probamos si el mes de febrero del año actual tiene 29 días
      if (checkdate(2,29,$anio_actual))
      {
        $bisiesto=true;
    }
    return $bisiesto;
}
function edad($fecha_nac){
	//
	$dia=date("j");
	$mes=date("n");
	$anno=date("Y");
	//descomponer fecha de nacimiento
	$anno_nac=substr($fecha_nac, 0, 4);
	$mes_nac=substr($fecha_nac, 5, 2);
	$dia_nac=substr($fecha_nac, 8, 2);
	//
	if($mes_nac>$mes){
        $calc_edad= $anno-$anno_nac-1;
        if($calc_edad >= 18){
            print " <br> Eres mayor";
        }    
	}else {
		if($mes==$mes_nac AND $dia_nac>$dia){
            $calc_edad= $anno-$anno_nac-1; 
        }else{
            $calc_edad= $anno-$anno_nac-1;
            if($calc_edad < 18){
                print " <br> Eres menor";
            }
        }
	}
    return $calc_edad;  
}
print " <br> Actualmente tengo ".edad("1999-05")." años";
?>