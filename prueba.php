<?php
// comentario de una linea
/* comentario
de
varias lineas
*/

//declaracion de
//no se declaran tipos de datos
$entera=10;
$decimal=1.10;
$string ="Variable de tipo string";
$booleanVerdadero=true;
$booleanFalso=false;

//imprimir
//concatenar en php  con . (punto)
echo "<h1 style='color:blue'>Variable entera" . $entera . "</h1>";
echo "Variable decimal $decimal";
echo "Variable string $string <br/>";

//declaracion de constantes en php
//a las constantes no se les antepone simpolo $
define("RENTA", "10%");
const IVA=1.13;

echo "Esto es una constante con define " . RENTA . "<br/>";
echo "Esto es una constante con const". IVA;

//variables globales y locales
$variableGlobal=25;

function prueba()
{
    global $variableGlobal;
    $variablelocal = 10;
    echo "<br/>Esto es una variable global: $variableGlobal";
    echo "<br/>Variable local: $variableLocal";
}

//llamado a la funcion
prueba();
echo $variableLocal;

?>